@extends('layout.master')
@section('content')

<style type="text/css">
	.container{
		margin-top: 50px;  
	}
</style>
   
 	        <!-- Error Message -->
            <!-- @include('error.error_message') -->

    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-8">
          <div class="shadow p-3 mb-3 bg-white rounded">
            <form  action="create" method="POST" enctype="multipart/form-data">
            		{{ csrf_field() }}
           		
          		 		<header>
          		 				<h2 class="text-primary text-center">@lang('article.title_article')</h2>
          		 		</header>
 		 
                 
                  <div class="form-group">
                    
                      <label>@lang('article.title')</label>         
                      <input type="text" class="form-control" id="title" name="title">
                    
                  </div>

                  

                  <div class="form-group">
                      <label> @lang('article.author')</label>         
                      <input type="text" class="form-control" id="author" name="author">
                 
                  </div>

                  <!-- Select Category -->
                  <div class="form-group">
                      <label>Category Article</label>        
                      <select class="form-control" name="category_id[]">
                        <option value="">--Select Option--</option>
                        @foreach($categorys as $cat)
                          <option  value="{{$cat->id}}">{{ $cat->name}}</option>
                        @endforeach
                      </select>
                   
                  </div>

                  <div class="form-group">                     
                      <label> @lang('article.description')</label> 
                      <textarea class="ckeditor" id="editor" name="description"></textarea>
                  </div>

                 
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-sm"> Submit </button>
                    <a href="{{url('/')}}" class="btn btn-success btn-sm">
                      <i class="fa fa-backward"></i>
                      Back </a>
                 
                </div>
            
          </form>
          </div>   <!-- close box-shadow-->
        </div>
      </div>
    </div>  <!-- close class container-->


    <!-- CKeditor -->
    <script type="text/javascript">  
      CKEDITOR.replace( 'editor' );  
  </script>  
    
@endsection
   


