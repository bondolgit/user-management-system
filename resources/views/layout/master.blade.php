<!DOCTYPE html>
<html>
<head>
	<title>Master Page</title> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <!-- Header -->
  


  <!-- DatePicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">

  <!-- CKeditor -->
  <script type="text/javascript" src="{{url('ckeditor1/ckeditor.js')}}"></script> 
  <!-- <script src="{{url('js/script.js')}}"></script> -->

   <!-- Add icon library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">





  <!-- Style Header -->
  <style type="text/css">
  ul,li,a {
    color: white;
    font-size: 15px;
    font-weight: bold;
  }
</style>

<style type="text/css">
  .container{
    margin-top: 50px;
  
  }
  table{
    margin-top: 30px;
    margin-bottom: 30px;
  }
</style>

</head>
<body>
		
		@include('layout.header')

		@yield('content')

		@include('layout.footer')

		
 
	
</body>
</html> 