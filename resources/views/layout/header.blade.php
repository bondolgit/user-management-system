 

      <!-- Header --> 
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-inf " style="background-color: #1c2331">
              <!-- Logo -->
              <a href="#"><img class="rounded-circle" src="./images/logo.jpg" width="100" height="100"> </a> 

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                      <li class="nav-item active">
                        <a class="nav-link" href="/">@lang('article.home')</a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link" href="/about">@lang('article.about_us')</a>
                      </li>

                       <li class="nav-item">
                        <a class="nav-link" href="/service">@lang('article.our_services')</a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link" href="/contact">@lang('article.contact_us')</a>
                      </li> 
                  </ul>

                  <div class="form-inline my-2 my-lg-0">
                      <ul class="navbar-nav mr-auto">
                          <li class="nav-item">
                            <a class="nav-link" href="/locale/en"><img src="{{asset('images/en.png')}}" width="30px" height="30px" />  English </a>
                          </li>

                          <li class="nav-item">
                            <a class="nav-link" href="/locale/kh"><img src="{{asset('images/kh.png')}}" width="30px" height="30px" /> Khmer </a>
                          </li>

                          <li class="nav-item">
                            <a class="nav-link" href="/locale/cn"><img src="{{asset('images/cn.jpg')}}" width="40px" height="30px" /> Chines </a>
                          </li>
                      </ul>
                  </div>
                </div>
            </nav> 
        </div>





      