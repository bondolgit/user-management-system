
@extends('layout.master')
@section('content')

<style type="text/css">
	.container{ 
		margin-top: 50px;
	}
</style>
  
 	

    <div class="container">
      <div class="row justify-content-center">
        <div class="col-sm-8">
          <div class="shadow p-3 mb-3 bg-white rounded">
        
            <form  action="{{url('/edit/'.$article->id)}}" method="POST" enctype="multipart/form-data">
              @method("PUT")

            		{{ csrf_field() }}
           		
        		 		<div class="col-md-12">
        		 				<h2 class="text-primary text-center">@lang('article.title_edit_article')</h2>
        		 		</div>
 		
                 
                  <div class="form-group">
                    
                      <label>@lang('article.title')</label>         
                      <input type="text" class="form-control" id="title" name="title" value="{{ $article->title }}">
               
                  </div>

        

                  <div class="form-group">
                      <label>@lang('article.author')</label>         
                      <input type="text" class="form-control" id="author" name="author" value="{{ $article->author }}">
                
                  </div>  
 
                

                <div class="form-group">
                      <label>@lang('article.description')</label>  
                      <textarea class="ckeditor" id="editor" name="description">
                          {!! $article->description !!}
                      </textarea>         
              
                  </div>
                 
                <div class="form-group">
              
                    <button type="submit" class="btn btn-primary btn-sm"> Submit </button>
                    <a href="{{url('/')}}" class="btn btn-primary btn-sm">
                      <i class="fa fa-backward"></i>
                      Back </a>
                 
                </div>
            
            </form>
          </div>  <!-- close shadow -->
        </div>
      </div>
    </div>  <!-- close class container-->

    <!-- CKeditor -->
    <script type="text/javascript">  
      CKEDITOR.replace( 'editor' );  
  </script> 
    
@endsection
   
