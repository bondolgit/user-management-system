

@if (count($errors) > 0)
    <!-- Form Error List -->
    <div class="alert alert-danger">        
        <strong>Whoops! Something went wrong!</strong>

        <br><br>

        <ul type="circle">                 
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('success'))
        <div class="alert alert-success">
            <p> {{  Session::get('success') }} </p>
       
    </div>
@endif
