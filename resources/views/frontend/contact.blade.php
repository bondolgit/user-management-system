
@extends('layout.master')
@section('content')
 
	<div class="container">
		<div class="row">
			<!-- form contact -->
			<div class="col-md-6">
				<div class="shadow p-3 mb-3 bg-white rounded">
					<h3 class="text-center text-primary">@lang('contact_us.contact_form')</h3>
					<form>
					<div class="form-group">
						<label>@lang('contact_us.username')</label>
						<input type="text" name="name" placeholder="Enter Username" class="form-control">
					</div>

					<div class="form-group">
						<label>@lang('contact_us.email')</label>
						<input type="email" name="email" placeholder="Enter Email" class="form-control">
					</div>

					<div class="form-group">
						<label>@lang('contact_us.message')</label>
						<textarea rows="5" cols="30" class="form-control"></textarea>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-success btn-sm">@lang('contact_us.submit_btn')</button>
					</div>
					</form>
				</div>
			</div>

			<!-- <div class="col-md-1"></div> -->
			<!-- Maps -->
			<div class="col-md-6">
				<div class="shadow p-3 mb-3 bg-white rounded">
					<h3 class="text-center text-primary">@lang('contact_us.map')</h3>
				
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d250151.1512034605!2d104.75009803674602!3d11.579666939586584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109513dc76a6be3%3A0x9c010ee85ab525bb!2z4Z6a4Z624Z6H4Z6S4Z624Z6T4Z644oCL4Z6X4Z-S4Z6T4Z-G4Z6W4Z-B4Z6J!5e0!3m2!1skm!2skh!4v1572711653497!5m2!1skm!2skh" width="500" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
				</div>
			</div>
		</div>
	</div>

@endsection