



<?php 

	return [

		// Menu
		'home' => '家',
		'about_us' => '關於我們',
		'contact_us' => '聯繫我們',
		'our_services' => '我們的服務',

		'title_article' => '建立文章',
		'title' => '標題',
		'description' => '描述',
		'author' => '作者',
		'date' => '日期',

		// Edit Article
		'title_edit_article' => '編輯文章',

		// List Article
		'title_list_article' => '列出文章',
		'action' => '行動',
            
	];​​​​
?>