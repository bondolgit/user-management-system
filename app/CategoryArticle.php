<?php

namespace App; 

use Illuminate\Database\Eloquent\Model;

class CategoryArticle extends Model
{
    //
    protected $table = "cetegory";

    // Articles

    public function articles(){
    	return $this->belongsToMany('App\Article','article_category','article_id','category_id');
    }
}
