

<?php 

	return [
		// Menu
		'home' => 'Home',
		'about_us' => 'About Us',
		'contact_us' => 'Contact Us',
		'our_services' => 'Our Services',
		// Create Article
		'title_article' => 'Create Article',
		'title' => 'Title',
		'description' => 'Description',
		'author' => 'Author',
		'date' => 'Date',

		// Edit Article
		'title_edit_article' => 'Edit Articles',
		// List Article
		'title_list_article' => 'List Articles',
		'action' => 'Action',
            
	];​​​​
?>