<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArrayObject extends Model
{
        public $id;
        public $name;
        public $gender;
        public $class;

        public function __construct($id,$name,$gender,$class){

                    $this->id = $id;
                    $this->name = $name;
                    $this->gender= $gender;
                    $this->class = $class;
        }        
        
}



