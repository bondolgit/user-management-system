
@extends('layout.master')
@section('content')


	<div class="container">
		<div class="row">
			<!-- Leftside -->
			<div class="col-md-9">
				<div class="row">

					<div class="col-md-5">
						<img src="./images/g33.jpg" width="400px" height="400px">
					</div>

					<div class="col-md-1"></div>

					<div class="col-md-6">
						<h3> @lang('about_us.about_title')</h3>
						<p> @lang('about_us.about_content')</p>
					</div>
				</div>
			</div>

			<!-- Sidebar -->
			<div class="col-md-3">
				<h3> @lang('article.contact_us') </h3>
				<span><b>@lang('about_us.address') : </b></span><span>Russey Keo,Kilometer 6,Phnome Penh</span><br><br>
				<span><b>@lang('about_us.email')  : </b></span><span>098 23 24 54 /012 324 555</span><br><br>
				<span><b>@lang('about_us.phone')  : </b></span><span>yembondolz@gmail.com</span><br><br>
				<span><b>@lang('about_us.website') : </b></span><span><a href="http://gigbcambodia.com/en" style="color:black;text-decoration: none">http://www.gigb.com</a></span>

				
				

			</div>
		</div>
	</div>

@endsection

