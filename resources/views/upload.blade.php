<!DOCTYPE html>
<html>
<head>
	<title></title> 
</head>
<body>
		<!-- enctype="multipart/form-data" : get parameter value on browser -->
		<!-- Single Upload -->
		<form action="/single_upload" method="POST" enctype="multipart/form-data">
			@csrf

			<input type="file" name="thumbnail">
			<input type="submit" value="Upload Now">
		</form>


		<!-- Multi Upload -->
		<!-- <form action="/multiple_upload_file" method="POST" enctype="multipart/form-data">
			@csrf

			<input type="file" name="thumbnail[]" multiple>
			<input type="submit" value="Upload Now">
		</form> -->
</body>
</html>