<?php
use App\ArrayObject;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/about',function(){
	return view('frontend.about_us');
});

Route::get('/contact',function(){
	return view('frontend.contact');
});

Route::get('/service',function(){
	return view('frontend.our_services');
});

Route::get('/show',function(){
	return view('show');
}); 

Route::get('get_show','MainController@show');
// Optional Name
Route::get('get_value/{name?}','MainController@get_paramet_val');
Route::get('file_extends','MainController@extends');
Route::get('array_string','MainController@array_string');

// Array 0f Object
Route::get('/array_object',function(){

	$obj = new ArrayObject("111","Bondol","Male","SLS");
	$obj1 = new ArrayObject("222","Darareaksmey","Male","A3");
	$obj2 = new ArrayObject("333","Maryna","Female","B3");
	$obj3 = new ArrayObject("444","Sovannara","Male","SLS");

	$result = [$obj,$obj1,$obj2,$obj3];
		return view("array_object")->with("array_object",$result);
});
 
// Condition
Route::get('/condition',function(){
	
	return view('condition')->with("a",0);
});

// Switch Language
Route::get('switch_lang','SwitchLanguageController@switch_language');

Route::get('locale/{locale}',function($locale){

	Session::put('locale',$locale);
		return redirect()->back(); 

		// this link will add session of lang when click to change
});
  
// Language HRD
Route::get('locale','SwitchLanguageController@lang_hrd'); 
// Route::get('locale/{locale}','SwitchLanguageController@dynamic_lang');

Route::get('get','SwitchLanguageController@get_request'); 

// CRUD  Article

Route::get('/','ArticleController@index')->name('index.article'); 
Route::get('/create','ArticleController@create');	
Route::post('/create','ArticleController@add');
Route::get('/edit/{id}','ArticleController@edit');
Route::put('/edit/{id}','ArticleController@update');
Route::post('/delete/{id}','ArticleController@delete');
Route::get('/delete_all','ArticleController@delete_all');
Route::get('/view/{id}','ArticleController@view');

// Form Search
Route::get('/search','ArticleController@search');

Route::get('/test','ArticleController@test'); 

// Category Article
Route::get('/index_category','CategoryArticleController@index')->name('index_category');
Route::get('/create_category','CategoryArticleController@get_create_category')->name('create_category');
Route::post('/create_category','CategoryArticleController@add_create_category')->name('create_category');
Route::get('/edit_category/{id}','CategoryArticleController@edit_category');
	
Route::post('/edit_category/{id}','CategoryArticleController@update_category');

// Upload File
Route::get('/upload','FileUploadController@upload');
Route::post('/single_upload','FileUploadController@action');
// Multiple File
Route::post('/multiple_upload_file','FileUploadController@uploadMultipleFile');

	

 