<?php 
   
namespace App\Http\Controllers; 

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Article;
use App\CategoryArticle;
use DB;
 

class ArticleController extends Controller
{
    //
    public function index(){

       
        // $keyword = "";
        $filters = ["ID","Title","Description","Author"];
        $search_filter = "";
        $articles = Article::paginate(5);
        $categorys = CategoryArticle::all();
        $count_row = Article::all()->count();
       
    		return view("article.index",compact("articles","count_row","categorys","filters","search_filter"));
    }

    public function create(Request $request){

        $categorys = CategoryArticle::all();
    	return view('article.create',compact("categorys"));
    }
    // Create
    public function add(Request $request){ 

        $this->validate($request,[

            'title' => 'required'
        ]); 

         
        $article = new Article();
        // 1. Mass Assignment
        $article->fill($request->all());
        $article->save();

            return redirect()->route('index.article')->with('success','Created Successfully');
        // $article->title = $request->title;
        // $article->description = $request->description;
        // $article->author = $request->author;
        // $article->created_at = date('Y-m-d');

        // $article_id = $article->save();

        // insert both_id to table="article_category"
        // if(count($article_id) > 0 ){
        //     foreach($request->category_id as $key=>$val){

        //         $article_category = [

        //             'article_id' => $article_id,
        //             'category_id'=> $request->category_id[$key],
        //         ];
        //         DB::table('article_category')->insert($article_category);
        //     }

        //     return redirect()->route('index.article')->with('success','Created Successfully');

        // } 
    }

    // Edit
    public function edit(Request $request,$id){

        $article = Article::find($id);
        $article_category = CategoryArticle::all();
            return view("article.edit",compact("article","article_category"));
    }


    public function update(Request $request,$id){

       Article::find($id)->update($request->all());

        // $article->title = $request->title;
        // $article->description = $request->description;
        // $article->author = $request->author;
        // $article->updated_at = date('Y-m-d');
        // $article_id = $article->save();


            return redirect()->route('index.article')->with('success','Updated Successfully');
        
    }

    // Delete 
    public function delete($id){

        $data = Article::find($id);
        $data->delete();
            return redirect()->route('index.article');
    }

    // Delete All rows
    public function delete_all(){

        Article::truncate();
    }

    // View
    public function view($id){

        $article = Article::find($id);
            return view('article.view',compact("article"));
    }

    // Form Search
    public function search(Request $request){

        // request from form search 
        $keyword = $request->keyword;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $search_filter = $request->search_filter; 
        $count = 0;
        $filters = ["ID","Title","Description","Author"];



            // if unselect get default is "title" to search
            if($search_filter == "0"){
                $search_filter = 'title';
            }
       
            // search_filter condition
            if($start_date == ""){
               
                $articles = Article::where($search_filter,'LIKE','%'.$keyword.'%')->paginate(3);
                $count = Article::where($search_filter,"LIKE","%".$keyword."%")->count();
            }else{
                
                $articles = Article::where($search_filter,'LIKE','%'.$keyword.'%')
                    ->whereBetween('created_at',[$start_date,$end_date])
                    ->paginate(3);
                    
                $count = Article::where($search_filter,"LIKE","%".$keyword."%")
                        ->whereBetween('created_at',[$start_date,$end_date])
                        ->count();
                        
            }

          
    
        $count_row = $articles->count();

       

            return view('article.index',compact("keyword","articles","count_row","start_date","end_date","filters","search_filter","count"));
    }

    public function test(){ 

        $articles = Article::all()->count();

        echo "The Number row is :".$articles;
    }
}











