
@extends('layout.master')
@section('content')

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">

			<a href="{{url('/')}}" class="btn btn-primary btn-sm">
				<i class="fa fa-backward"></i>
                      Back </a>

				<table class="table table-bordered table-striped">
					<thead style="background-color: #98FB98">
                      <tr>
                        
                        <th class="text-center"><h4><b>បរិយាយ</b></h4></th>
                        <th></th>
                      </tr>
                    </thead>

                    <tbody>
						<tr>
							<td><b>ID</b></td>
							<td>{{ $article->id}}</td>
						</tr>

						<tr>
							<td><b>Title</b></td>
							<td>{{ $article->title}}</td>
						</tr>

						<tr>
							<td><b>Description</b></td>
							<td>{{ $article->description}}</td>
						</tr>

						<tr>
							<td><b>Author</b></td>
							<td>{{ $article->author}}</td>
						</tr>
					</tbody>
				</table>
			
			</div>
		</div>
	</div>


	
@endsection