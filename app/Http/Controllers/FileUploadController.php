<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    //
    public function upload(){

    	return view('upload');  
    }

    // Action upload
    public function action(Request $request){
    // 1.
    	// $file = $request->file("thumbnail");
    		// // if doesn't have this folder it will create in public folder
    	// $destination = "FileUpload"; 
    	// $fileName = $file->getClientOriginalName();		
    	// $file->move($destination,$fileName);

    // 2. 
    	$file = $request->file("thumbnail");
    	// go to storage/app/public/Create folder: FileUpload/our image
    		// it generate image to protect duplicate file
    	$destination = "public/FileUpload";   
    	$file->store($destination);
    }

    // Upload Multipl File
    public function uploadMultipleFile(Request $request){

    	$files = $request->file("thumbnail");
    	$destination = "FileUpload";

    	foreach($files as $file){
 
    		$fileName = $file->getClientOriginalName();		
    		$file->move($destination,$fileName);
    	}

    }
}
