<?php

namespace App\Http\Middleware;

use Closure;
use App;

class Localization 
{ 
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Session::has('locale')){
            // Set language  in Our Application
            App::setLocale(\Session::get('locale'));

            // Default language is Eng
            // this middleware will change language by session

        }
        return $next($request);
    }
}
