

<footer style="background-color: #1c2331 ;height: auto">
	<style>
		ul,li{
			color: white;
			font-size: 15px;
		}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<h4 class="text-secondary">Programming Language</h4>
				<ul type="circle">
					<li>JAVA</li>
					<li>PHP</li>
					<li>C#</li>
					<li>ASP.NET</li>
					<li>Javascript</li>
					<li>C++</li>
					<li>C programming</li>
				</ul>
			</div>
			<div class="col-md-3">
				<h4 class="text-danger">PHP Framework</h4>
				<ul type="square">
					<li>Laravel</li>
					<li>Codginitor</li>
					<li>Yii</li>
					<li>Syforny</li>
					<li>CakePHP</li>
					<li>Slim</li>
					
				</ul>
			</div>

			<div class="col-md-3">
				<h4 class="text-success">JS Framework</h4>
				<ul>
					<li>Angular</li>
					<li>React</li>
					<li>Vue</li>
					<li>Jquery</li>
					<li>Node.js</li>
					<li>Polymer</li>
					
				</ul>
			</div>

			<div class="col-md-3">
				<h4 class="text-primary">Search Form</h4>
					<input class="form-control" type="text" placeholder="Search" aria-label="Search">
			</div>
		</div>
	</div>	 
</footer>