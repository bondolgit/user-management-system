@extends('layout.master')
@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Slide -->
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<img src="./images/slide_service2.png" width="100%">
			</div>
		</div>
	</div>

	<!-- Our Services Title -->
	<div class="container">
		<div class="row">
			<div class="col-md-12">
					<h1 class="text-center text-primary">@lang('our_services.service_title')</h1>
					<p class="text-center">@lang('our_services.service_content')</p>

			</div>
		</div>
	</div>

	<!-- Services Content -->
	<div class="container">
		<div class="row">
			<div class="col-md-2" style="background-color: #DCDCDC">
				<center>
					<i class="fa fa-cog" style="font-size:50px;"></i><br><br>
				
					<label><b class="text-center">Web Design</b></label>
				
					<p>Through our years experienced IT experts, we have developed two important Websites and Online Systems for the Ministry of Tourism and this </p>
					<button class="btn btn-primary btn-sm">Learn More</button>
				</center>
			</div>
			<div class="col-md-1"></div>

			<div class="col-md-2" style="background-color: #DCDCDC">
				<center>
					<i class="fa fa-desktop" style="font-size:50px"></i><br><br>
				
					<label><b class="text-center">Web Development</b></label>
				
					<p>Through our years experienced IT experts, we have developed two important Websites and Online Systems for the Ministry of Tourism and this s</p>
					<button class="btn btn-primary btn-sm">Learn More</button>
				</center>
			</div>
			<div class="col-md-1"></div>

			<div class="col-md-2" style="background-color: #DCDCDC">
				<center>
					<i class="fa fa-bank" style="font-size:50px"></i><br><br>
				
					<label><b class="text-center">Web Hosting</b></label>
				
					<p>Through our years experienced IT experts, we have developed two important Websites and Online Systems for the Ministry of Tourism and this </p>
					<button class="btn btn-primary btn-sm">Learn More</button>
				</center>
			</div>
			<div class="col-md-1"></div>

			<div class="col-md-2" style="background-color: #DCDCDC">
				<center>
					<i class="fa fa-wrench" style="font-size:50px"></i><br><br>
				
					<label><b class="text-center">Software Consulting</b> </label>
				
					<p>Through our years experienced IT experts, we have developed two important Websites and Online Systems for the Ministry of Tourism and this </p>
					<button class="btn btn-primary btn-sm">Learn More</button>
				</center>
			</div>
		
		</div>
	</div>

@endsection