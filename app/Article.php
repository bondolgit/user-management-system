<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	//

    //  Filable
    public $fillable = ['title','description','author'];  

    // Guarded
    // public $guarded = ['_token'];

    // Category Article
    // public function category_article(){

    // 	return $this->belongsToMany('App\CategoryArticle','article_category','article_id','category_id');
    // }
   
}
