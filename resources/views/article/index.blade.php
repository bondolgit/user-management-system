@extends('layout.master')
@section('content')


<style>
    
    /*btn hover*/
    .btn:hover {

  		background-color: RoyalBlue;
	}

  </style>
  
  


<!-- Form Search --> 

	<div class="container">
	    <form action="/search" method="GET" role="search">
	       
	        <div class="row">

	        	<!-- Search Filter -->
	            <div class="col-md-2">
	                 <div class="form-group">
	                 	<label><b>Search Filter</b> </label>
	                    <select class="form-control" name="search_filter" style="background:whitesmoke;color:black;font-weight: bold">
	                    	<option value="0">---Select Option---</option>
	                    	@foreach($filters as $f)
	                    		<option value="{{$f}}" 

	                    			@if($f == $search_filter)
	                    				selected="selected"; 
	                    			@endif

	                    		>{{$f}}</option>
	                    	@endforeach
	                    	
	                    </select>
	                       
	                </div>
	            </div>
	            <!-- Search Keyword -->
	            <div class="col-md-4">
	                 <div class="form-group">
	                 	<label> <b>Search Keyword</b></label>
	                    <input type="text" class="form-control" name="keyword"
	                        value="{{ isset($keyword) ? $keyword : '' }}"> <span class="input-group-btn">

	                       
	                </div>
	            </div>

	     		
	            <div class="col-md-3">
	            	<div class="form-group input-daterange">
	            		<label><b>Start Date</b></label>
	            		<input type="text" name="start_date" id="datepicker1" class="form-control" placeholder="YYYY-MM-DD" autocomplete="off" />
	            		<!-- <span class="input-group-addon">
                        	<span class="glyphicon glyphicon-calendar"></span>
                    	</span> -->
					 
	            	</div>

	            </div>

	            <div class="col-md-3">
	                 <div class="form-group input-daterange">
	                 	<label><b>End Date</b></label>
	                    <input type="text" name="end_date" id="datepicker2" class="form-control"  placeholder="YYYY-MM-DD"	 autocomplete="off" />
	                </div>
	            </div>

	            <div class="form-group">

	            	<button type="submit" class="btn btn-success btn-sm" name="submit_search"><i class="fa fa-search"></i>Search</button>
	            	<button type="reset" class="btn btn-danger btn-sm">Clear</button>
	            	
	            </div>
	        </div>
	    </form>
	</div>




	<!-- ======= Content ====== -->
	
	<div class="container">
		
		<h2 class="text-center text-success"> @lang('article.title_list_article') </h2>
		<table class="table table-bordered table-hover">
			<a href="{{url('create')}}" class="btn btn-success btn-sm"><i class="fa fa-plus-circle"></i>New Article</a>
			<a href="{{url('/delete_all')}}" class="btn btn-danger btn-sm delete_all"  style="float:right"><i class="fa fa-close"></i>delete all</a>
			<tr class="bg-primary">
				<th>ID</th>
				<th>@lang('article.title')</th>
				<!-- <th>@lang('article.description')</th> -->
				<th>@lang('article.author')</th>
				<th>@lang('article.date')</th>
				<th>@lang('article.action')</th>
			</tr>

			@if(count($articles) > 0)	 
				@foreach($articles as $a)

			<tr>
				<td>{{$a->id}}</td>
				<td>{{$a->title}}</td>
				<!-- <td>{!! str_limit($a->description,20) !!}</td> -->
				<td>{{$a->author}}</td>
				<td>{{ date("Y-m-d",strtotime($a->created_at)) }}</td>
				<td>

					<a href="{{url('/view/'.$a->id)}}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
					<a href="{{url('/edit/'.$a->id)}}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
					<form action="{{url('/delete/'.$a->id)}}" method="POST" enctype="multipart/form-data" style="display:inline-block;">
						{{ csrf_field() }}

						<button type="submit" class="btn btn-danger btn_delete btn-sm">
							<i class="fa fa-trash"></i></button>
					</form>

				</td>	
			</tr>

				@endforeach 

			@else

	 			<h2 class="text-danger">Data Not Found</h2>
				
			@endif
			 

			 <tr>
			 	<td colspan="3"><b>Total Row Number:</b></td>
			 	<td colspan="3" class="text-center"><b style="color:red;">{{ $count_row }}</b></td>
			 </tr>
		</table>	

				<!-- Paginate -->
				<center>
					{{ $articles->appends(request()->all())->links() }}
				</center>
	</div>



	<!-- Alert Message before Delete -->
	<script type="text/javascript">

		$("document").ready(function(){
			$(".btn_delete").click(function(){
				return confirm("Do you want to delete this field!!");
			});
			//delete all rows
			$(".delete_all").click(function(){
				return confirm("Do you want to delete all records!!");
			});
		});

		
	</script>

	<!-- <script type="text/javascript">

	//start_date
    $('#datepicker1').datepicker({  

       format: 'yyyy-mm-dd'

     });  
    // end_date
    $('#datepicker2').datepicker({  

       format: 'yyyy-mm-dd'

     }); 

</script>   -->

	<!-- DatePicker -->
	<script>
	   // Start Date
	  $( function() {
	    $( "#datepicker1" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
	  } );

	  // End date
	  $( function() {
	    $( "#datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' }).val();
	  } );

  </script>

 
@endsection






  


 