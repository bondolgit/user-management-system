@extends('layout.master')
@section('content')

<style type="text/css">
    .container{
        margin-top: 30px;
    }
</style>

<div class="container">
    <form action="/search" method="POST" role="search">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                 <div class="input-group">
                    <input type="text" class="form-control" name="q"
                        placeholder="Search By Title and Description"> <span class="input-group-btn">
                        <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-search"></span>
                            Submit
                        </button>
                    </span>
                </div>
            </div>
            <div class="col-md-6"></div>
        </div>
    </form>
</div>


<div class="container">
    @if(isset($details))
        <p> The Search results for your query <b> {{ $query }} </b> are :</p>
    <h2>Sample Article details</h2>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th>Author</th>
            </tr>
        </thead>
        <tbody>
            @foreach($details as $art)
            <tr>
                <td>{{$art->id}}</td>
                <td>{{$art->title}}</td>
                <td>{{$art->description}}</td>
                <td>{{$art->author}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endif
</div>

@endsection