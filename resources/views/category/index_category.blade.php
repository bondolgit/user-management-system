
@extends('layout.master')
@section('content')

 

	
	<div class="container">
		
		<h2 class="text-center text-success"> List Category </h2>
		<table class="table table-bordered table-hover">
			<a href="{{url('create_category')}}" class="btn btn-success btn-sm">Create Category</a>

			<tr class="bg-primary">
				<th>ID</th>
				<th>Category Name</th>
				<th>Description</th>
				<th>Action</th>
				
			</tr>

			@if(count($categorys) > 0)	
				@foreach($categorys as $cat)

			<tr>
				<td>{{$cat->id}}</td>
				<td>{{$cat->name}}</td>
				<td>{{$cat->description}}</td>
				
				<td>

					
					<a href="{{url('/edit_category/'.$cat->id)}}" class="btn btn-info btn-sm">Edit</a>
					<form action="#" method="POST" enctype="multipart/form-data" style="display:inline-block;">
						{{ csrf_field() }}

						<button type="submit" class="btn btn-danger btn_delete btn-sm">Delete</button>
					</form>

				</td>	
			</tr>

				@endforeach
			@endif
		</table>		
	</div>
	
@endsection