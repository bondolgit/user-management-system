


<?php 

	return [

		'home' => 'ទំព័រដើម',
		'about_us' => 'អំពី​ពួក​យើង',
		'contact_us' => 'ទំនាក់ទំនងពួក​យើង',
		'our_services' => 'សេវាកម្មរបស់យើង',
		// Create Article
		'title_article' => 'បង្កើតអត្ថបទ',
		'title' => 'ចំណងជើង',
		'description' => 'ការពិពណ៌នា',
		'author' => 'អ្នកនិពន្ធ',
		'date' => 'កាលបរិច្ឆេទ',

		// Edit Article
		'title_edit_article' => 'កែប្រែអត្ថបទ',

		// List Article
		'title_list_article' => 'បញ្ជីអត្ថបទ',
		'action' => 'សកម្មភាព',
            
	];​​​​
?>