
@extends('layout.master')
@section('content')
  
	<div class="container">
		<div class="row">
			<div class="col-md-10">
				<form action="{{url('create_category')}}" method="POST" enctype="multipart/form-data">
					@csrf

					<div class="col-md-12">
		 				<h2 class="text-primary text-center">Lists Category</h2>
		 			</div>

					<div class="form-group">
	                    <div class="col-sm-2"></div>
	                    <div class="col-sm-10"> 
	                      <label>Category Name</label>         
	                      <input type="text" class="form-control" id="category" name="category">
	                    </div>
                  	</div>

					<div class="form-group">
	                    <div class="col-sm-2"></div>
	                    <div class="col-sm-10"> 
	                      <label>Description</label>         
	                      <textarea class="form-control" name="description" rows="5" cols="30"></textarea>
	                    </div>
                  	</div>

					<div class="form-group">
						<button type="submit" class="btn btn-success btn-sm">Submit</button>
						<a href="{{route('index_category')}}" class="btn btn-primary btn-sm">
                      Back </a>
					</div>

				</form>
			</div>
		</div>
	</div>

@endsection