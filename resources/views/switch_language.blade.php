<!doctype html>
<html lang="{{app()->getLocale()}}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="#">@lang('home.home_menu') <span class="sr-only">(current)</span></a>
                    </li>

                    <li class="nav-item">
                      <a class="nav-link" href="#">@lang('home.about_menu')</a>
                    </li>

                    <li class="nav-item">
                      <a class="nav-link" href="#">@lang('home.contact_menu')</a>
                    </li>

                   
                  </ul>

                  <div class="form-inline my-2 my-lg-0">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                          <a class="nav-link" href="locale/en"><img src="{{asset('images/en.png')}}" width="30px" height="30px" />  English </a>
                        </li>

                        <li class="nav-item">
                          <a class="nav-link" href="locale/kh"><img src="{{asset('images/kh.png')}}" width="30px" height="30px" /> Khmer </a>
                        </li>

                        <li class="nav-item">
                          <a class="nav-link" href="locale/cn"><img src="{{asset('images/cn.jpg')}}" width="40px" height="30px" /> Chines </a>
                        </li>
                    </ul>
                  </div>
                </div>
            </nav> 
        </div>

        <!-- Content -->
        <div class="containe">
          <div class="row">
            <div class="col-md-12">
                <center>
                     <h1> @lang('home.message') </h1>
                    <p>@lang('home.school_name') </p>
                    <p>@lang('home.department') </p> 
                    <p>@lang('home.old_work') </p>
                    <p>@lang('home.current_work') </p>
                    
                </center>
                 
                
            </div>
          </div>
        </div>


     
    
  </body>
</html>