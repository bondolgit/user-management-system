<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryArticle;

class CategoryArticleController extends Controller
{
    //index
    public function index(){

        $categorys = CategoryArticle::all();
            return view('category.index_category',compact("categorys"));
    }
     
    // Create
    public function get_create_category(Request $request){

    	return view('category.create_category');
    }

    // Create

    public function add_create_category(Request $request){


    	$this->validate($request,[

    		'category' => 'required',
    		'description' => 'required' 
    	]);

    	$category = new CategoryArticle();
    	$category->name = $request->category;
    	$category->description = $request->description;
    	$category->created_at = date('Y-m-d');
    	$category->save();
            return redirect()->route('index_category');

    }
 
    // Edit
    public function edit_category(Request $request, $id){

        $category = CategoryArticle::find($id);
            return view('category.edit_category',compact("category"));
    }

    // Update
    public function update_category(Request $request,$id){

        $category = CategoryArticle::find($id);
        $category->name = $request->category;
        $category->description = $request->description;
        $category->updated_at = date('Y-m-d');
        $category->save();
            return redirect()->route('index_category');



    }

}
